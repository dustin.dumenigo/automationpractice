# automationpractice

Automatiza 5 casos de prueba utilizando el patrón Page Object y las assertions correspondientes.
Utiliza Data Driven Testing para ejecutar uno de los casos de prueba con 2 sets de datos.
Agrega reportes con Allure.
Incorporar regresion visual.
